import random
from Comb import Combinaison

#Création de la classe Player

class Player:
    
    def pseudo():
        player = input("Saisissez un pseudo avec plus de 5 caractères ou un vous sera commis d'office : ")
        if(len(player) < 5):
            player = f"Player{random.randint(1000,9999)}"
        return player
    
    def __init__(self,pseudo = pseudo()):
        self.pseudo = pseudo
        self.solde = 500
        self.cards = []
        self.combinaison = Combinaison()
        self.in_party = True
        self.bet = False
        print(f"Bienvenue {self.pseudo}!")
        
    def addCard(self,card):
        self.cards.append(card)
        
        
    def getCards(self):
        cards = []
        for i in self.cards:
            cards.append(i.getCard())
        return cards
    
    def getPseudo(self):
        return self.pseudo
    
    def phaseChoice(self,state="check"):
        match state:
            case "check":
                r = input("Enter 'F' to fold 'C' to check and 'R' to raise : ")
                while(r not in ['f','F','c','C','r','R']):
                    r = input("Enter 'F' to fold 'C' to check and 'R' to raise : ")
                if(r in ["f","F"]):
                    self.in_party = False
                    print(f"{self.getPseudo()} fold !")
                elif(r in ['r','R']):
                    self.bet = True
                    self.solde -= 100
                    print(f"{self.getPseudo()} raise 100$ !")
                else:
                    print(f"{self.getPseudo()} check !")
                    
            case "call":
                r = input("Enter 'F' to fold and 'C' to call : ")
                while(r not in ['f','F','c','C','r','R']):
                    r = input("Enter 'F' to fold and 'C' to call : ")
                if(r in ["f","F"]):
                    self.in_party = False
                    print(f"{self.getPseudo()} fold !")
                else:
                    self.bet = True
                    self.solde -= 100
                    print(f"{self.getPseudo()} call !")
        
              

        
    def getComb(self,table_cards):
        cards = table_cards + self.cards
        self.combinaison.comb(cards)
        print(f"{self.getPseudo()} has a : {self.combinaison.texte} : {self.combinaison.combinaison}")

    def resetBet(self): self.bet = False
    def reset(self):
        self.cards = []
        self.in_party = True
        self.bet = False
    