import random
from Comb import Combinaison
#Création de la classe Bot

class Bot:
    
    def __init__(self,name):
        self.name = name
        self.solde = 500
        self.cards = []
        self.combinaison = Combinaison()
        self.bet = True
        self.in_party = True
        
        
    def addCard(self,card):
        self.cards.append(card)
        
    def getCards(self):
        cards = []
        for i in self.cards:
            cards.append(i.getCard())
        return cards
    
    def getPseudo(self):
        return self.name
    
    def getComb(self,table_cards):
        cards = table_cards + self.cards
        self.combinaison.comb(cards)
        print(f"{self.getPseudo()} has a : {self.combinaison.texte} : {self.combinaison.combinaison}")

    def phaseChoice(self, state="check"):
        match state:
            case "check":
                choice = [1,1,2,1,0,2,0,1,0,1]
                r = random.choice(choice)
                if (r == 0):
                    self.in_party = False
                    print(f"{self.getPseudo()} fold !")
                elif (r == 2):
                    self.bet = True
                    self.solde -= 100
                    print(f"{self.getPseudo()} raise 100$ !")
                else:
                    print(f"{self.getPseudo()} check !")

            case "call":
                choice = [1,0,0,1,1,1,0,1,0,1]
                r = random.choice(choice)
                if (r == 0):
                    self.in_party = False
                    print(f"{self.getPseudo()} fold !")
                else:
                    self.bet = True
                    self.solde -= 100
                    print(f"{self.getPseudo()} call !")

    def resetBet(self):
        self.bet = False
        
    def reset(self):
        self.cards = []
        if(self.solde > 200):
            self.in_party = True
        else:
            self.in_party = False