# Jeu Mastermind 


Ce jeu a été réalisé afin de tester mes compétences logiques et applicatives en ``Python``.

## Cas d'utilisation

```plantuml

@startuml
actor Player
rectangle PokerTable {
  Player --> (Deal)
  (Deal) --> (FirstBettingRound)
  (FirstBettingRound) --> (DealFlop)
  (DealFlop) --> (SecondBettingRound)
  (SecondBettingRound) --> (DealTurn)
  (DealTurn) --> (ThirdBettingRound)
  (ThirdBettingRound) --> (DealRiver)
  (DealRiver) --> (FourthBettingRound)
  (FourthBettingRound) --> (Showdown)
  (Showdown) --> (Winner)
}
@enduml


```

## Pour commencer

1. Connaître les règles du jeu.
2. Transformer les règles en code


### Pré-requis

Avoir un environnement de développement en ``Python ``


### Installation

Télécharger le répertoire
Ou le fichier [Poker.exe](/out/Poker.exe)

## Démarrage

Exécuter le fichier ``Poker.exe`` ou ouvrez le répertoire dans votre IDE

## Aperçu

Aperçu du démarrage :

![Aperçu du début](assets/ap1.jpg)

Aperçu du premier tour de table :

![Aperçu du premier tour de table](assets/ap2.jpg)

Aperçu du reste de la partie :

![Aperçu du reste de la partie](assets/ap3.jpg)

## Fabriqué avec

* [Thonny](https://thonny.org/) - IDE de développement en Python
* [PyCharm](https://www.jetbrains.com/fr-fr/pycharm/) - IDE de développement Python
* [Python](https://www.python.org/) - Langage de programmation interprété


## Auteurs

* **Joackim SEGOR** _alias_ [@TheJoker971](https://gitlab.com/TheJoker971)

