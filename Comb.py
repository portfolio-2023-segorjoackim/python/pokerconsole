#Création de la class Combinaisons

class Combinaison:
    
    def __init__(self):
        self.value = 0
        self.texte = ""
        self.combinaison = []
        self.kicker = ()
        self.value_kicker = 0
        self.low_value = 0
        
        
    def orderDesc(self,cards):
        for i in range(len(cards)):
            for j in range(len(cards)):
                if(cards[i].value < cards[j].value):
                    petit = cards[i]
                    cards[i] = cards[j]
                    cards[j] = petit
        return cards
    
    def orderCardsNoDouble(self,cards):
        liste = []
        cards = self.orderDesc(cards)
        for i in range(len(cards)):
            for j in range(i+1,len(cards)):
                if(cards[i].value == cards[j].value and i != j):
                    liste.append(cards[i])
        for i in liste:
            if(i in cards):
                cards.remove(i)
        #ordre =[]
        #for i in cards:
            #ordre.append(i.value)
        #print(ordre)
        return cards
        
        
            
    def order_cards(self,all_cards):
        for i in range(len(all_cards)):
            for j in range(len(all_cards)):
                if(all_cards[i].value < all_cards[j].value):
                    petit = all_cards[j]
                    all_cards[j] = all_cards[i]
                    all_cards[i] = petit
        #ordre = []
        #for i in all_cards:
            #ordre.append(i.value)
        #print(ordre)
                    
        return all_cards
    
    def getTexte(self):
        return self.texte
                    
    def comb(self,cards):
        cards = self.order_cards(cards)
        def high(cards):
            self.combinaison = cards[len(cards)-1].getCard()
            self.value = 1
            self.texte ="High card"
            self.kicker = self.combinaison
            self.value_kicker = cards[len(cards)-1].value

        
        def paire(cards):
            combinaison = []
            for i in range(len(cards)-1,-1,-1):
                if(cards[i].value == cards[i-1].value):
                    self.kicker = cards[i].getCard()
                    self.value_kicker = cards[i].value
                    combinaison.append(cards[i].getCard())
                    combinaison.append(cards[i-1].getCard())
                    break
            if(len(combinaison) == 2):
                self.combinaison = combinaison
                self.value = 2
                self.texte ="Paire"
                
            
        
        def double(cards):
            combinaison = []
            v =[]
            for i in range(len(cards)-1,-1,-1):
                if(cards[i].value == cards[i-1].value and cards[i].value not in v and len(combinaison) != 4):
                    v.append(cards[i].value)
                    combinaison.append(cards[i].getCard())
                    combinaison.append(cards[i-1].getCard())
            if(len(combinaison) == 4):
                self.combinaison = combinaison
                self.value = 3
                self.texte ="Double Paire"
                self.kicker = combinaison[0]
                self.value_kicker = v[0]
                self.low_value = v[len(v)-1]
                 
        def three(cards):
            combinaison = []
            for i in range(len(cards)-1,1,-1):
                if(cards[i].value == cards[i-1].value == cards[i-2].value):
                    combinaison.append(cards[i].getCard())
                    combinaison.append(cards[i-1].getCard())
                    combinaison.append(cards[i-2].getCard())
                    self.value_kicker = cards[i].value
                    break
            if(len(combinaison) == 3):
                self.combinaison = combinaison
                self.value = 4
                self.texte = "Three of a Kind"
                self.kicker = combinaison[0]
                
        def straight(cards):
            combinaison =[]
            cards = self.orderCardsNoDouble(cards)
            if(len(cards) >= 5):
                for i in range(len(cards)-4):
                    if(cards[i].value+1 == cards[i+1].value and cards[i+1].value+1 == cards[i+2].value and  cards[i+2].value+1 == cards[i+3].value
                       and cards[i+3].value+1 == cards[i+4].value):
                        combinaison.append(cards[i].getCard())
                        combinaison.append(cards[i+1].getCard())
                        combinaison.append(cards[i+2].getCard())
                        combinaison.append(cards[i+3].getCard())
                        combinaison.append(cards[i+4].getCard())
                        self.value_kicker = cards[i+4].value
                
                        
                if(len(combinaison) == 5):
                    self.combinaison = combinaison
                    self.value = 5
                    self.texte = "Straight"
                    self.kicker = combinaison[4]
                
        def flush(cards):
            combinaison = []
            for i in range(len(cards)-1,3,-1):
                if(cards[i].family == cards[i-1].family == cards[i-2].family == cards[i-3].family == cards[i-4].family):
                    combinaison.append(cards[i].getCard())
                    combinaison.append(cards[i-1].getCard())
                    combinaison.append(cards[i-2].getCard())
                    combinaison.append(cards[i-3].getCard())
                    combinaison.append(cards[i-4].getCard())
                    self.value_kicker = cards[i].value
                    break
            if(len(combinaison) == 5):
                self.combinaison = combinaison
                self.value = 6
                self.texte = "Flush"
                self.kicker = combinaison[0]
                
        def full(cards):
            combinaison = []
            for i in range(len(cards)-1,1,-1):
                if(cards[i].value == cards[i-1].value == cards[i-2].value):
                    combinaison.append(cards[i].getCard())
                    combinaison.append(cards[i-1].getCard())
                    combinaison.append(cards[i-2].getCard())
                    cards.remove(cards[i])
                    cards.remove(cards[i-1])
                    cards.remove(cards[i-2])
                    self.value_kicker = cards[i].value
                    break
            if(len(combinaison) == 3):
                for i in range(len(cards)-1,-1,-1):
                    if(cards[i].value == cards[i-1].value):
                        combinaison.append(cards[i].getCard())
                        combinaison.append(cards[i-1].getCard())
                        self.low_value = cards[i].value
                        break
            if(len(combinaison) == 5):
                self.combinaison = combinaison
                self.value = 7
                self.texte = "Fullhouse"
                self.kicker = combinaison[0]
            
        def four(cards):
            combinaison =[]
            for i in range(len(cards)-1,2,-1):
                if(cards[i].value == cards[i-1].value == cards[i-2].value == cards[i-3].value):
                    combinaison.append(cards[i].getCard())
                    combinaison.append(cards[i-1].getCard())
                    combinaison.append(cards[i-2].getCard())
                    combinaison.append(cards[i-3].getCard())
                    self.value_kicker = cards[i].value
                    break
            if(len(combinaison) == 4):
                self.combinaison = combinaison
                self.value = 8
                self.texte = "Four of a Kind"
                self.kicker = combinaison[0]
    
        def stFlush(cards):
            combinaison =[]
            cards = self.orderCardsNoDouble(cards)
            if(len(cards) >= 5):
                for i in range(len(cards)-4):
                    if(cards[i].value+1 == cards[i+1].value and cards[i+1].value+1 == cards[i+2].value and cards[i+2].value+1 == cards[i+3].value
                       and cards[i+3].value+1 == cards[i+4].value):
                        if(cards[i].family == cards[i-1].family == cards[i-2].family == cards[i-3].family == cards[i-4].family):
                            combinaison.append(cards[i].getCard())
                            combinaison.append(cards[i+1].getCard())
                            combinaison.append(cards[i+2].getCard())
                            combinaison.append(cards[i+3].getCard())
                            combinaison.append(cards[i+4].getCard())
                            self.value_kicker = cards[i+4].value
                            break
                if(len(combinaison) == 5):
                    self.combinaison = combinaison
                    self.value = 9
                    self.texte = "Straight Flush"
                    self.kicker = combinaison[4]
            
        def royale(cards):
            combinaison = []
            cards = self.orderCardsNoDouble(cards)
            if(len(cards) >= 5):
                for i in range(len(cards)-4):
                    if([cards[i].value,cards[i+1].value,cards[i+2].value,cards[i+3].value,cards[i+4].value] == [10,11,12,13,14]):
                        if(cards[i].family == cards[i+1].family == cards[i+2].family == cards[i+3].family == cards[i+4].family):
                            combinaison.append(cards[i].getCard())
                            combinaison.append(cards[i+1].getCard())
                            combinaison.append(cards[i+2].getCard())
                            combinaison.append(cards[i+3].getCard())
                            combinaison.append(cards[i+4].getCard())
                            break
                if(len(combinaison) == 5):
                    self.combinaison = combinaison
                    self.value = 10
                    self.texte = "Royale Straight Flush"
                

                    
                    
        liste = [high(cards),paire(cards),double(cards),three(cards),straight(cards),flush(cards),full(cards),four(cards),stFlush(cards),royale(cards)]
        
    
        
        