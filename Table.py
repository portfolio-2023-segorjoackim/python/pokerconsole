from Pack import Pack

#Création de la classe Table

class Table:
    
    def __init__(self):
        self.cards = []
        self.players = []
        self.in_party = []
        self.bet = 0
        self.gains = 0
        self.pack = Pack()
        
    def addCard(self,card):
        self.cards.append(card)
        
        
    def getCards(self):
        cards = []
        for i in self.cards:
            cards.append(i.getCard())
        return cards
    
    def addPlayers(self,players):
        if(type(players) == list):
            for i in players:
                self.players.append(i)
        else:
            self.players.append(players)
            
    def getPlayers(self):
        players =[]
        for i in self.players:
            players.append(i.getPseudo())
        return players
    
    def getComb(self):
        for i in self.in_party:
            i.getComb(self.cards)
            print("-"*55)
    
    def inParty(self):
        for i in self.players:
            if(i.in_party == True and i not in self.in_party):
                self.in_party.append(i)
            else:
                if(i in self.in_party and i.in_party == False):
                    self.in_party.remove(i)
        
    def turnPhase(self):
        self.inParty()
        self.resetBet()
        if(len(self.in_party) >= 2):
            for i in self.in_party:
                if(self.bet > 0):
                    print(f"The bet is of : {self.bet}!")
                    for j in self.in_party:
                        if(j.bet == False):
                            j.phaseChoice("call")
                        if (j.bet == True and j != i):
                            self.bet += 100
                        if(len(self.in_party) == 1):
                            break
                    break
                else:
                    i.phaseChoice()
                    self.inParty()
                    if(i.bet == True):
                        self.bet += 100
                if(len(self.in_party) == 1):
                    break
        self.gains += self.bet
        self.resetBet()

    def resetBet(self):
        for i in self.in_party:
            if(i.bet == True):
                i.resetBet()
        self.bet = 0

    def reset(self):
        self.pack.reset()
        self.cards = []
        self.in_party = []
        for i in self.players:
            i.reset()
        self.gains = 0
            
            