from Player import Player
from Bot import Bot
from Table import Table



#Création de la classe Game

class Game:
    
    def setting():
        n_players = 1
        n_gamers = int(input("Saisissez le nombre de joueurs autour de la table (2,4,6,8) :"))
        while(n_gamers not in [2,4,6,8]):
            n_gamers = int(input("Saisissez le nombre de joueurs autour de la table (2,4,6,8)"))
        return n_players,n_gamers
    
    def __init__(self,settings = setting()):
        self.settings = settings
        self.table = Table()
        print("\n"*10)
        self.initSettings()
        self.playGame()
        
    def initSettings(self):
        for i in range(self.settings[0]):
            globals()[f"player{i+1}"] = Player()
            self.table.addPlayers(globals()[f"player{i+1}"])
        for i in range(self.settings[1]-1):
            globals()[f"bot{i+1}"] = Bot(f"Bot{i+1}")
            self.table.addPlayers(globals()[f"bot{i+1}"])

    def ordre(self):
        order = []
        for i in self.table.in_party:
            order.append(i)
        for i in range(len(order)):
            for j in range(i, len(order)):
                if (order[i].combinaison.value > order[j].combinaison.value):
                    petit = order[j]
                    order[j] = order[i]
                    order[i] = petit

                elif (order[i].combinaison.value == order[j].combinaison.value and order[i].combinaison.value_kicker > order[j].combinaison.value_kicker):
                    petit = order[j]
                    order[j] = order[i]
                    order[i] = petit
                elif (order[i].combinaison.value == order[j].combinaison.value and order[i].combinaison.value_kicker == order[j].combinaison.value_kicker and order[i].combinaison.low_value > order[j].combinaison.low_value):
                    petit = order[j]
                    order[j] = order[i]
                    order[i] = petit
        return order
    def winnerParty(self):
        p = self.ordre()
        i = p[len(p)-1]
        delete =[]
        for j in p:
            if(j!=i):
                if(j.combinaison.value != i.combinaison.value):
                    if(j not in delete):
                        delete.append(j)
                elif(j.combinaison.value == i.combinaison.value and j.combinaison.value_kicker != i.combinaison.value_kicker):
                    if(j not in delete):
                        delete.append(j)
                elif(j.combinaison.value == i.combinaison.value and j.combinaison.value_kicker == i.combinaison.value_kicker and j.combinaison.low_value != i.combinaison.low_value):
                    if(j not in delete):
                        delete.append(j)

        for d in delete:
            p.remove(d)
        if(len(p) == 1):
            p[0].solde += self.table.gains / 2
            print(f"Winner is {p[0].getPseudo()} with a {p[0].combinaison.texte} : {p[0].combinaison.combinaison} Kicker : {p[0].combinaison.kicker}\nHe won {self.table.gains}$ !")
        elif(len(p) == 2):
            p[0].solde += self.table.gains/2
            p[1].solde += self.table.gains/2
            print(f"Egality enter {p[0].getPseudo()} and {p[1].getPseudo()} with a {p[0].combinaison.texte} : {p[0].combinaison.combinaison}\nThey won {self.table.gains/2}$ !")
        else:
            for i in self.table.in_party:
                i.solde += self.gains/len(self.table.in_party)
            print("No winner !")




            
      
    
    def playGame(self):
        self.table.inParty()
        while(self.table.players[0].solde != 0 and len(self.table.in_party)>1):
            next_turn = False
            while(len(self.table.in_party) > 1 and next_turn == False):
                for i in self.table.in_party:
                    i.solde -= 100
                    self.table.gains += 100

                self.table.pack.giveCards(self.table.players,2)
                p = self.table.players[0]
                print(f"{p.getPseudo()} possède : {p.getCards()}")
                print("-"*55)

                print("$"*50,f"\nYou can win {self.table.gains}$ !\n","$"*50)
                self.table.pack.giveCards(self.table,3)
                print(self.table.getCards())
                print("-"*55)
                self.table.turnPhase()

                if(len(self.table.in_party) > 1):
                    for i in range(2):
                        print("$" * 50, f"\nYou can win {self.table.gains}$ !\n", "$" * 50)
                        self.table.pack.giveCards(self.table)
                        print(self.table.getCards())
                        self.table.turnPhase()
                        print("-"*60)
                        if(len(self.table.in_party) < 2):
                            break
                    next_turn = True
                
                
            if(len(self.table.in_party) == 1):
                print(f"Winner is {self.table.in_party[0].getPseudo()} !\nHe won {self.table.gains}$ !")
            elif(len(self.table.in_party) > 1):
                self.table.getComb()
                self.winnerParty()
            self.resetParty()
            self.table.inParty()
            print("\n" * 10)
    
    def resetParty(self):
        self.table.reset()

        
        
Game()
        

        