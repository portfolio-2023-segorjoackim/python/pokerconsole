import random
from Card import Card

#Crétion de la class Pack

class Pack:
    def pack():
        family = ["Heart","Spades","Tiles","Trefles"]
        name = ["2","3","4","5","6","7","8","9","10","J","Q","K","As"]
        value = [2,3,4,5,6,7,8,9,10,11,12,13,14]
        all_cards =[]
        for i in family:
            for j in range(len(name)):
                all_cards.append(Card(i,name[j],value[j]))
        return all_cards
    
    def __init__(self,pack1 = pack(),pack2 = pack()):
        self.cards = pack2
        self.remaining_cards = pack1
        
        
    
    def giveCards(self,who,n=1):
        if(type(who) == list):
            for j in range(n):
                for i in who:
                    card = random.choice(self.remaining_cards)
                    self.remaining_cards.remove(card)
                    i.addCard(card)

        else:
            for j in range(n):
                card = random.choice(self.remaining_cards)
                self.remaining_cards.remove(card)
                who.addCard(card)

#Remettre les cartes
    def reset(self):
        cards = []
        for i in self.remaining_cards:
            cards.append(i.getCard())
        for i in self.cards:
           if(i.getCard() not in cards):
               self.remaining_cards.append(i)
               
        
